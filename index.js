// alert("Hello World");


/*
	1. Create a function named getUserInfo which is able to return an object. 
	
		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/

function getUserInfo() {
		
		let userInfo = {
			name: "Jonas Alarcon",
			age: "25",
			address: "Coron Palawan",
			isMarried:"false",
			petName: "jhads"
		};

		return userInfo;

	};

	let getUser = getUserInfo();
	console.log(getUser);


/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/
function getArtistsArray() {
		
		let ArtistsArray = [
			"linkin park", "Bruno mars", "flow-g", "My chimecal Romance", "The Weeknd"
		];

		return ArtistsArray;

	};

	let getArtist = getArtistsArray();
	console.log(getArtist);



/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/
	function getSongsArray() {
		
		let SongsArray = [
			"creep", "billionaire", "praning", "I dont love you", "father and son"
		];

		return SongsArray;

	};

	let getSong = getSongsArray();
	console.log(getSong);




/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/
function getMoviesArray() {
		
		let MoviesArray = [
			"Master of sex", "Breaking bad", "As good as it gets", "3 idiots", "crazy, stupid, love"
		];

		return MoviesArray;

	};

	let getMovies = getMoviesArray();
	console.log(getMovies);



/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/
function getPrimeNumberArray() {
		
		let NumberArray = [
			5, 10, 15, 20, 25, 
		];

		return NumberArray;

	};

	let getPrime = getPrimeNumberArray();
	console.log(getPrime);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}